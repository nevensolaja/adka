



$(document).ready(function(){


    //Slider Banner
    $('.js-banner-slider').slick({
        infinite:true,
        slidesToShow:1,
        slidesToScroll:1,
        dots:false,
        speed:500,
        arrows:true,
        autoplay:false,
        prevArrow:$('.js-arrow-prev'),
        nextArrow:$('.js-arrow-next'),
    });

    
    //Accordion menu

    $('.accordion__box-head').on('click', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).parent().find('.accordion__box-body').hide();
            return false;
        }
        $('.accordion__box-head').each(function(){
            $(this).removeClass('active');
            $(this).parent().find('.accordion__box-body').hide();
        });
        $(this).addClass('active');
        $(this).parent().find('.accordion__box-body').show();
    })
        

    //Toggle hamburger menu  //

    $('.js-hamburger').on('click', function(){
        $(this).toggleClass('hamburger--active');
        $('.js-nav').toggleClass('nav__wrapp--active');
        $('.js-nav-logo').toggleClass('nav__logo-img--active');
        $('.js-nav-link').toggleClass('nav__list-item-link--active');
        $('.js-nav-request').toggleClass('nav__request-wrap--active');
    })
});